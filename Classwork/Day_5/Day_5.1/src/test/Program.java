package test;
class Test{
	private int num1;
	private int num2;
	private static int num3;
	static {	//Static Intializer Block
		System.out.println("Inside static initializer block of class Test");
		Test.num3 = 500;
	}
	public Test( int num1, int num2 ) {
		System.out.println("Inside constructor of class Test");
		this.num1 = num1;
		this.num2 = num2;
	}
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
		System.out.println("Num3	:	"+Test.num3);
	}
}
public class Program {
	static {	//Static Intializer Block
		System.out.println("Inside static initializer block of class Program");
	}
	public static void main(String[] args) {
		
		
		
		System.out.println("Inside main method");
			
		Test t1 = new Test( 10, 20 );
		Test t2 = new Test( 30, 40 );
		Test t3 = new Test( 50, 60 );
		
		t1.print();
		t2.print();
		t3.print();
	}
}
