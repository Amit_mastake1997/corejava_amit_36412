package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptArray(int[] arr) {
		if( arr != null ) {
			for( int index = 0; index < arr.length; ++ index ) {
				System.out.print("arr[ "+index+" ]	:	");
				arr[ index ] = sc.nextInt();
			}
		}
	}
	private static void printArray(int[] arr) {
		if( arr != null ) {
			for( int index = 0; index < arr.length; ++ index )
				System.out.print(arr[ index ]+"	");
			System.out.println();
		}
	}
	public static void main(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30, 40, 50 };	
		//System.out.println(arr.toString());	//[I@42a57993
		System.out.println(Arrays.toString(arr));	//[10, 20, 30, 40, 50]
	}
	public static void main11(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30, 40, 50 };	
		for( int element : arr )	//forward only and read only loop
			System.out.println(element);
	}
	public static void main10(String[] args) {
		int[] arr = new int[ ] { 50, 10, 40, 30, 20 };	
		Program.printArray(arr);
		
		Arrays.sort(arr);	//Dual Pivot Quicksort
		Program.printArray(arr);
	}
	public static void main9(String[] args) {
		int[] arr = new int[ ] { 10, 20, 30 };	//OK
		//int value = arr[ 1 ];
		//int value = arr[ -1 ];	//ArrayIndexOutOfBoundsException
		//int value = arr[ arr.length ];	//ArrayIndexOutOfBoundsException
		int value = arr[ 10 ];	//ArrayIndexOutOfBoundsException
		System.out.println("Value	:	"+value);
	}
	public static void main8(String[] args) {
		//int[] arr = new int[ 3 ] { 10, 20, 30 };	//Not OK
		//int[] arr = new int[ ] { 10, 20, 30 };	//OK
		//int[] arr = new int[ ];	//Not OK
		int[] arr = new int[ 3 ];	//OK
		Program.printArray(arr);
	}
	public static void main7(String[] args) {
		int[] arr = new int[ 3 ];
		Program.acceptArray(arr);
		Program.printArray(arr);
	}
	public static void main6(String[] args) {
		Object[] arr = new String[ 3 ]; 
		arr[ 0 ] = new String("DAC");
		arr[ 1 ] = new Integer(10);	//ArrayStoreException
	}
	public static void main5(String[] args) {
		int[] arr = new int[ 3 ]; 	//OK
		Program.printArray( arr );
	}
	public static void main4(String[] args) {
		int[] arr = new int[ 3 ]; 	//OK
		for( int index = 0; index < 3; ++ index )
			System.out.println(arr[ index ] );
	}
	public static void main3(String[] args) {
		int[] arr = new int[ -3 ]; 	//NegativeArraySizeException
	}
	public static void main2(String[] args) {
		int[] arr = null;
		arr = new int[ 3 ];
		
		//int[] arr = new int[ 3 ]; //OK
		//int arr[] = new int[ 3 ]; //OK
	}
	public static void main1(String[] args) {
		//new int[ 3 ];
		//int arr[ ];	//OK
		//int[] arr;	//OK
		//int [ arr ];	//Not OK
	}
}
	
