package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		double[] arr = new double[ 3 ];	//Array of primitive type
		System.out.println(Arrays.toString(arr)); //[0.0, 0.0, 0.0]
	}
	public static void main2(String[] args) {
		int[] arr = new int[ 3 ];	//Array of primitive type
		System.out.println(Arrays.toString(arr)); //[0, 0, 0]
	} 
	public static void main1(String[] args) {
		boolean[] arr = new boolean[ 3 ];	//Array of primitive type
		System.out.println(Arrays.toString(arr)); //[false, false, false]
	} 
}
	
