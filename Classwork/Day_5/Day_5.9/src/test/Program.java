package test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		final int num1 = 10;	//num1 : compile time constant
		System.out.println("Num1	:	"+num1);
		
		System.out.print("Num2	:	");
		final int num2 = sc.nextInt();	//num2 : run time constant
		System.out.println("Num2	:	"+num2);
	}
	public static void main3(String[] args) {
		final int number = 10;	//OK
		//number = 20;	//Not OK
		System.out.println("Number	:	"+number);
	}
	public static void main2(String[] args) {
		final int number;	//OK
		number = 10;	//OK
		System.out.println("Number	:	"+number);
	}
	public static void main1(String[] args) {
		final int number = 10;	//OK
		//number = number + 1;	//Not OK
		System.out.println("Number	:	"+number);
	}
}
	
