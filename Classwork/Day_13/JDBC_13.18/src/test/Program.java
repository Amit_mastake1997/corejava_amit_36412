package test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pojo.Book;
import utils.DBUtils;

public class Program {
	public static void main(String[] args) {
		String sql = "{call sp_insert_book(?,?,?,?,?)}";
		try (Connection connection = DBUtils.getConnection();
				CallableStatement statement = connection.prepareCall(sql)) {

			int bookId = 1025;
			String subjectName = "OS", bookName = "Operating System Concept's", authorName = "Galvin";
			float price = 575.50f;

			statement.setInt(1, bookId);
			statement.setString(2, subjectName);
			statement.setString(3, bookName);
			statement.setString(4, authorName);
			statement.setFloat(5, price);

			boolean result = statement.execute();
			if (result == false) {
				int count = statement.getUpdateCount();
				System.out.println(count + " row(s) affected.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
