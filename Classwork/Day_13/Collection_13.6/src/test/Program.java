package test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Program {
	public static List<Integer> getIntegerList( ){
		List<Integer> list = new ArrayList<Integer>( );
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(10);
		list.add(20);
		list.add(30);
		list.add(10);

		return list;
	}
	public static void main(String[] args) {
		List<Integer> list = Program.getIntegerList();		
		//list.forEach(System.out::println);
		Set<Integer> set = new LinkedHashSet<Integer>(list);
		list.clear();
		list.addAll(set);
		for( Integer element : list )
			System.out.println(element);
	}
}
