package test;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class Program {
	public static void main(String[] args) {
		Map<Integer, String> map = new TreeMap<>();
		map.put(34, "DAC");
		map.put(25, "PREDAC");
		map.put(12, "DMC");
		map.put(50, "DMC");
		
		Set<Entry<Integer, String>> entries = map.entrySet();
		for (Entry<Integer, String> entry : entries) {
			System.out.println(entry.getKey()+"	"+entry.getValue());
		}
	}
}
