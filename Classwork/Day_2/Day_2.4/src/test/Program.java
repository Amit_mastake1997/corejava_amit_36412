package test;
public class Program {
	public static void main(String[] args) {
		String str = "1a2b5"; 
		int number = Integer.parseInt(str);  //NumberFormatException
		System.out.println("Number	:	"+number);
	}
	public static void main4(String[] args) {
		String str = "125"; //Non Primitive
		int number = Integer.parseInt(str); //Primitive : UnBoxing
		System.out.println("Number	:	"+number);
	}
	public static void main3(String[] args) {
		double number = 10.5;
		//String strNumber = Double.toString(number);//Non Primitive : Boxing
		String strNumber = String.valueOf(number);//Non Primitive : Boxing
		System.out.println("Number	:	"+strNumber);
	}
	public static void main2(String[] args) {
		int number = 10;	//Primitive
		//String strNumber = Integer.toString(number); //Non Primitive : Boxing
		String strNumber = String.valueOf(number);//Non Primitive : Boxing
		System.out.println("Number	:	"+strNumber);
	}
	public static void main1(String[] args) {
		char ch = 'A';
		//String str = Character.toString(ch);//Non Primitive : Boxing
		String str = String.valueOf(ch);//Non Primitive : Boxing
		System.out.println(str);
	}
}
