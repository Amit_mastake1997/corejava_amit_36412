package test;
class Date implements Cloneable{
	private int day;
	private int month;
	private int year;
	public Date() {
		this(0,0,0);
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Date clone( ) throws CloneNotSupportedException {
		Date other = (Date) super.clone();//Creating new instance from exisiting instance
		return other;
	}
	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}
}
class Employee implements Cloneable{
	private String name;
	private int empid;
	private float salary;
	private Date joindate;
	public Employee() {
		this("",0,0, new Date());
	}
	public Employee(String name, int empid, float salary, Date joindate) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
		this.joindate = joindate;
	}
	public Date getJoindate() {
		return joindate;
	}
	public Employee clone( ) throws CloneNotSupportedException {
		Employee other = (Employee) super.clone();
		other.name = new String( this.name);
		other.joindate = this.joindate.clone();
		return other;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + ", joindate=" + joindate + "]";
	}
}
public class Program {	
	public static void main(String[] args) {
		try {
			Employee emp1 = new Employee("Sandeep", 33, 45000.50f, new Date(26,12,2006));
			Employee emp2 = emp1.clone();	//Shallow Copy of references;
			
			emp1.getJoindate().setDay(1);
			emp1.getJoindate().setMonth(1);
			emp1.getJoindate().setYear(2007);
			
			System.out.println(emp1.toString());
			System.out.println(emp2.toString());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	
	}
	public static void main3(String[] args) {
		Employee emp1 = new Employee("Sandeep", 33, 45000.50f, new Date(26,12,206));
		Employee emp2 = emp1;	//Shallow Copy of references;
		if( emp1 == emp2 )
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
	}
	public static void main2(String[] args) {
		Employee emp1 = new Employee("Sandeep", 33, 45000.50f, new Date(26,12,206));
		Employee emp2 = emp1;	//Shallow Copy of references;
		if( emp1 == emp2 )
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
	}
	public static void main1(String[] args) {
		try {
			Date dt1 = new Date( 6,11,2020);
			Date dt2 = dt1.clone();	
			if( dt1 == dt2 )
				System.out.println("Equal");
			else
				System.out.println("Not Equal");
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
