package test;
public class Program {	//Program.class
	public static void main(String[] args) {
		//Object obj;	//obj => reference
		//new Object();	//new Object(); => Anonymous Instance
		//Object obj = new Object( );	//Instance with reference
		Object obj = new Object( ) {	//Program$1.class
			private String message = "Inside anonymous inner class";
			@Override
			public String toString() {
				return this.message;
			}
		};
		String message = obj.toString();
		System.out.println(message);
	}
}
