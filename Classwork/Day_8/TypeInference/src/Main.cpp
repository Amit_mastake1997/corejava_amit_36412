#include<iostream>
using namespace std;

template<class T>
void swap_object( T &a, T &b){
	T temp = a;
	a = b;
	b = temp;
}
int main( void ){

	int x = 10;
	int y = 20;
	//swap_object<int>(x, y);
	swap_object(x, y);	//swap_object<int>(x, y);
	cout<<"X	:	"<<x<<endl;
	cout<<"Y	:	"<<y<<endl;
	return 0;
}
