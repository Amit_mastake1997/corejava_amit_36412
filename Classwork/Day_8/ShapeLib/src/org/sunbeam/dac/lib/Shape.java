package org.sunbeam.dac.lib;

public abstract class Shape {
	protected float area;

	public Shape() {
	}

	public abstract void calculateArea();

	public float getArea() {
		return area;
	}
}