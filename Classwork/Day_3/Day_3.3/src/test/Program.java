package test;

import java.util.Scanner;

class Account{
	//Field
	private int number;		//0
	private String type;	//null
	private float balance;	//0.0
	public Account() {
		this(1105, "Current", 85000);	//Constructor Chaining
	}
	public Account(int number, String type, float balance) {
		this.number = number;
		this.type = type;
		this.balance = balance;
	}
	public void printRecord(  ) {
		System.out.println("Number	:	"+this.number);
		System.out.println("Type	:	"+this.type);
		System.out.println("Balance	:	"+this.balance);
	}
}
public class Program {
	public static void main(String[] args) {
		Account account = new Account(  );
		account.printRecord( );	
	}
	public static void main1(String[] args) {
		Account account = new Account( 1010, "Loan", 2500000.0f );
		account.printRecord( );	
	}
}
