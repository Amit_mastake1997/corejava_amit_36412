package test;

import java.util.Scanner;

class Account{
	//Field
	private int number;		//0
	private String type;	//null
	private float balance;	//0.0
	public void acceptRecord( /* Account this */ ) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Number	:	");
		this.number = sc.nextInt();
		System.out.print("Type	:	");
		sc.nextLine();
		this.type = sc.nextLine();
		System.out.print("Balance	:	");
		this.balance = sc.nextFloat();
	}
	public void printRecord( /* Account this */  ) {
		System.out.println("Number	:	"+this.number);
		System.out.println("Type	:	"+this.type);
		System.out.println("Balance	:	"+this.balance);
	}
}
public class Program {
	public static void main(String[] args) {
		Account account = new Account( );
		account.acceptRecord( );	//account.acceptRecord( account );
		account.printRecord( );		//account.printRecord( account );
	}
	public static void main1(String[] args) {
		//new Account();	//Annonymous instance
		Account account = new Account( );
	}
}
