package assignment;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter a five letter word"); 
		
		String input = sc.nextLine();

		for (int i = 0; i < input.length(); i++) {             
		    for (int j = 0; j < input.length(); j++) {        
		        for (int k = 0; k < input.length(); k++) {     
		            if (i == j || i == k || j == k) continue;  
		            System.out.printf("%c%c%c\n", input.charAt(i), input.charAt(j), input.charAt(k));
		        }
		    }
		}
	}
}