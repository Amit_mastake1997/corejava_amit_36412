package assignment;

import java.util.Scanner;
public class Program
{   
	public static int index(String str1,String str2)
	{
		int length = str1.length() - str2.length();
		for (int i = 0; i <= length; i++) 
		{
				boolean match = true;
				int count=i;
				for (int j = 0; j < str2.length(); j++) 
				{
					if (str1.charAt(count) != str2.charAt(j)) 
					{
						match = false;
						break;
					}
					count++;
				}
				if(match==true)
					return i;
		}
		return -1;
	}
	public static int lastIndex(String str1, String str2)
	{
		int returns = -1;
		for(int i = str1.length()-1; i>= str2.length()-1; i--)
		{
			if(str2.equals(str1.substring(i-str2.length()+1,i+1)))
			{
				returns = i-str2.length()+1;
				break;
			}
		}
		return returns;
	}
	public static void main( String args[] ) 
	{
		Scanner scanner = new Scanner( System.in );
		System.out.println( "Enter a sentence : " );
		String sentence = scanner.nextLine();
		System.out.println( "Enter any word in sentence : " );
		String word = scanner.nextLine();
		
		System.out.println("Index Of '"+word+"' is :"+index(sentence,word));
		//System.out.println("Index Of '"+word+"' is :"+sentence.indexOf(word));
		
		System.out.println("Last Index Of '"+word+"' is :"+lastIndex(sentence,word));
		//System.out.println("Last Index Of '"+word+"' is :"+sentence.lastIndexOf(word));
	 scanner.close();
	}
}