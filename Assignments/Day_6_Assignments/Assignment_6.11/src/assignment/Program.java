package assignment;

import java.util.Scanner;
import java.util.*;
public class Program {

		 public static void main(String[] args) {
			 Scanner input = new Scanner(System.in);
			 String date;
			 String m;
			 int day,month,year;

			 String[] totalMonths = {"","January","February",
			"March", "April", "May", "June", "July", "August",
			"September", "October", "November", "December" };
			 System.out.println("\n Input Date mm/dd/yyyy ");
			 date = input.nextLine();
			 StringTokenizer token = new
			 StringTokenizer(date,"/");
			 month = Integer.parseInt( token.nextToken() );
			 day = Integer.parseInt ( token.nextToken());
			 year = Integer.parseInt ( token.nextToken());
			 m = totalMonths[ month ];

			 System.out.println( m + " " + day + ", " + year );
		 }
	 }
