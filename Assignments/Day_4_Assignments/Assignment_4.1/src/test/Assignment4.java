package test;

import java.util.Scanner;
public class Assignment4 {
	static Scanner sc = new Scanner(System.in);
	private static void acceptArray(int[][] arr) {
		if( arr != null ) {
			for(  int rcount = 4; rcount>=1; -- rcount ) {
				for( int ccount = 1; ccount<=rcount; ++ ccount ) {
					System.out.print("arr[ "+rcount+" ][ "+ccount+" ]	:	");
					arr[ rcount ][ ccount ] = sc.nextInt();
				}
			}
		}
	}
	private static void printArray(int[][] arr) {
		if( arr != null ) {
			for( int rcount = 4; rcount>=1; --rcount ) {
				for(int ccount = 1; ccount <= rcount; ++ccount ) {
					System.out.print(arr[ rcount ][ ccount ]+"	");
				}
				System.out.println();
			}
		}
	}
public static void main(String[] args) {
	int[][] arr = new int[ 5 ][ 5 ];
	Assignment4.acceptArray( arr );
	System.out.println("");
	Assignment4.printArray(arr);
}
}
