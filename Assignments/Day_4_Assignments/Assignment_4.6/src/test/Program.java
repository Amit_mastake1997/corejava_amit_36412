package test;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double amount, dollar, rupee, code;
		DecimalFormat f = new DecimalFormat("##.##");
		Scanner sc = new Scanner(System.in);

		System.out.println("Currency Converter!");
		System.out.println("which currency You want to Convert ");
		System.out.println("1:Ruppe \n2:Dollar");

		code = sc.nextInt();

		System.out.println("How much Money you want to convert");

		amount = sc.nextFloat();

		if (code == 1) {

			dollar = amount / 70;
			System.out.println("Your " + amount + " Rupee is : " + f.format(dollar) + " Dollar");
		}

		else if (code == 2) {
			rupee = amount * 70;
			System.out.println("Your " + amount + " Dollar is : " + f.format(rupee) + " Ruppes");
		} else {
			System.out.println("Invalid input");
		}

		System.out.println("Thank you ");
		sc.close();
	}

}
