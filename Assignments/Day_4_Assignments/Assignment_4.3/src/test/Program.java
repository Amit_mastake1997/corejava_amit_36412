package test;

import java.util.Scanner;


public class Program {

	public void calculateSales() {
		
		
		double productTotal = 0.0;
		int column, row;
		
		
		Scanner input = new Scanner(System.in);
		
		// Sales array data on products sold by salesman
		double sales[][] = new double[5][4];

		System.out.print("Enter sales person number:");
		int person = input.nextInt();

		while (person != -1) {
			System.out.print("Enter product number:");
			int product = input.nextInt();
			System.out.print("Enter sales amount:");
			double amount = input.nextDouble();

			// error check input
			if (person >= 1 && person < 5 && product >= 1 && product < 6 && amount >= 0)
				sales[product - 1][person - 1] = +amount;
			else
				System.out.println("Invalid Input!");

			System.out.print("Enter sales person number");
			person = input.nextInt();
		} 
		
		// salesperson totals
		double salesPersonTotal[] = new double[4];

		// display data table
		for (column = 0; column < 4; column++)
			salesPersonTotal[column] = 0;

		System.out.printf("%8s%14s%14s%14s%14s%10s\n", "Product", "Salesperson_1", "Salesperson_2", "Salesperson_3",
				"Salesperson_4", "Total");

		// Printing a person's sales of a product
		for (row = 0; row < 5; row++) {
			productTotal = 0.0;
			System.out.printf("%8d%", (row + 1));
		}

		for (column = 0; column < 4; column++) {
			System.out.printf("%14.2f", sales[row][column]);
			productTotal = productTotal + sales[row][column];
			salesPersonTotal[column] = salesPersonTotal[column] + sales[row][column];
		}

		System.out.printf("8%s", "Total");

		for (column = 0; column < 4; column++)
			System.out.printf("14.2%f", salesPersonTotal[column]);

		System.out.println();
		input.close();
	}
}
