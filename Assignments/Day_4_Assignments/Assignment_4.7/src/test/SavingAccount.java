package test;

public class SavingAccount {
	private static float annuallInterestRate;
    private float savingsBalance;
    private float monthlyInterest;

    public SavingAccount(float savingsBalance) {
        this.savingsBalance = savingsBalance;

    }

    public void calculateMonthlyInterest() {
        this.monthlyInterest = (savingsBalance * annuallInterestRate) / 12;
        System.out.println("The monthly interest is : " + this.monthlyInterest);
    }

    public static void modifyInterestRate(float interestRate) {
        annuallInterestRate = interestRate;
    }

    private void calculateSavings() {
        savingsBalance += this.monthlyInterest;
    }

    public void displaySavings() {
        calculateSavings();
        System.out.println("The total balance is : " + savingsBalance);
    }
}
