package test;

public class Account{

    
    public static void main(String[] args) {
     
       
        SavingAccount saver1=new SavingAccount(2000);
        SavingAccount saver2=new SavingAccount(3000);
       
        SavingAccount.modifyInterestRate(4);
		
        saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		saver1.displaySavings();
		saver2.displaySavings();
		
        SavingAccount.modifyInterestRate(5);
        
		saver1.calculateMonthlyInterest();
		saver2.calculateMonthlyInterest();
		saver1.displaySavings();
		saver2.displaySavings();
          
          
    }
    
}
