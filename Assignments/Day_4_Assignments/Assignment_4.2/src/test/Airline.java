package test;

import java.util.Scanner;

public class Airline {

	private int fseat = 1;
	private int Sseat = 6;

	Scanner input = new Scanner(System.in);

	public void firstClass() {

		if (fseat <= 5) {

			System.out.println("Seat Reserved in First class \nSeat " + fseat);

		} else {

			System.out.println("No Seats Available In First Class  ");

		}

	}

	public void secondClass() {

		if (Sseat <= 10) {

			System.out.println("Seat Reserved in Second class \nSeat " + Sseat);

		} else {

			System.out.println("No Seats Available In Second Class ");

		}

	}

	public void action() {

	   int fseatFound=0;
		do {
			
			System.out.print("1 > First Class \n2 > Second Class\n3 To Exit\n : ");

			int get = input.nextInt();
			switch (get) {
			case 1:
				firstClass();
				++fseat;
				break;
			case 2:
				secondClass();
				++Sseat;
				break;

			case 3:
				System.out.println("Thanks For Booking ");
				System.exit(0);
				break;
			default:
				System.out.println("\nInvalid Input ");
				action();
				break;
			}
             ++fseatFound;
		} while (fseatFound < 10);
		System.out.println("No Seats Avialable  Next Flight Leaves In 3hrs ");
	}

}
