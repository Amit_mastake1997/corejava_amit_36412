package org.sunbeam.dac.test;

import org.sunbeam.dac.assignment5.Address;
import org.sunbeam.dac.assignment5.Date;
import org.sunbeam.dac.assignment5.Person;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Date date = new Date();
		System.out.println(date.toString());
		
		Address address = new Address( "Kolhapur", "MH", 416115);
		System.out.println(address.toString());
		
		
		Person person = new Person("Amit",02,11,2020,"Ichalakranji","Maharastra",416115);
		System.out.println(person.toString());
		
	}

}
