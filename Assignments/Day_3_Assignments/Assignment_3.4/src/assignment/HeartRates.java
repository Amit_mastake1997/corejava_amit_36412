package assignment;

public class HeartRates {

	private String firstName;
	   private String lastName;
	   private int birthYear;
	    
	   
	public HeartRates() {
		firstName=null;
		lastName=null;
		birthYear=0;
	}


	public HeartRates(String firstName, String lastName, int birthYear) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthYear = birthYear;
	}


	   public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}
}
