package assignment;

import java.util.Scanner;
import java.util.Calendar;

public class TestHeartRates {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		HeartRates myHealth = new HeartRates();
		
		Calendar cal = Calendar.getInstance();
		int k = cal.get(Calendar.YEAR);

		System.out.print("Person's First Name: ");
		String first = sc.nextLine();
		myHealth.setFirstName(first);

		System.out.print("Person's Last Name: ");
		String last = sc.nextLine();
		myHealth.setLastName(last);

		System.out.print("Person's Year of birth: ");
		int birthY = sc.nextInt();
		myHealth.setBirthYear(birthY);

		
		System.out.printf("First Name: %s%n", first);
		System.out.printf("Last Name: %s%n", last);


		int age = k - birthY;
		System.out.printf("Age: %d%n", age);

		
		int maxHeartRate = 220 - age;
		System.out.printf("Maximum Heart Rate: %d%n", maxHeartRate);


		double targetMin = maxHeartRate * 0.50;
		double targetMax = maxHeartRate * 0.85;
		
		
		System.out.printf("Target heart rate range: %.0f", targetMin);
		System.out.printf(" - %.0f", targetMax);
		
		sc.close();
	}
}