package assignment;




import java.util.Scanner;


public class InvoiceTest {
	private Invoice invoice = new Invoice();
	
	
	static Scanner sc = new Scanner(System.in);

	public  void acceptRecord() {
		
		System.out.print("\nEnter part number :");
		sc.nextLine();
		this.invoice.setPartNumber(sc.nextLine());

		
		System.out.print("Enter part description :");
		//sc.nextLine();
		this.invoice.setPartDescription(sc.nextLine());

		System.out.print("Enter item purchased :");
		this.invoice.setItemPurchased(sc.nextInt());
		
		System.out.print("Enter price per item :");
		this.invoice.setPricePerItem(sc.nextDouble());
	}

	public  void printRecord() {
		System.out.print("\nDetail of items purchasing–>");

		System.out.print("\nPart number :" + this.invoice.getPartNumber());

		System.out.print("\nPart description :" + this.invoice.getPartDescription());

		System.out.print(" \nTotal Billing Amount :"  + this.invoice.getInvoiceAmount());
	}

	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Accept Record");
		System.out.println("2.Print Record");
		System.out.print("Enter choice	:	");
		int choice = sc.nextInt();
		return choice;
	}

}