package assignment;

public class Invoice {

	private String partNumber;

	private String partDescription;

	private int itemPurchased;

	private double pricePerItem;

	public Invoice() {

		partNumber = "";

		partDescription = "";

		itemPurchased = 0;

		pricePerItem = 0.0;

	}

	public Invoice(String partNumber, String partDescription, int itemPurchased, double pricePerItem) {
		super();
		this.partNumber = partNumber;
		this.partDescription = partDescription;
		this.itemPurchased = itemPurchased;
		this.pricePerItem = pricePerItem;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}

	public int getItemPurchased() {
		return itemPurchased;
	}

	public void setItemPurchased(int itemPurchased) {
		this.itemPurchased = itemPurchased;
	}

	public double getPricePerItem() {
		return pricePerItem;
	}

	public void setPricePerItem(double pricePerItem) {
		this.pricePerItem = pricePerItem;
	}

	double getInvoiceAmount() {

		return (itemPurchased * pricePerItem);

	}

}