package assignment;

import java.util.Scanner;

public class EmployeeTest {
	
	private Employee e1 = new Employee();
	private Employee e2 = new Employee();

	static Scanner sc = new Scanner(System.in);

	public void acceptRecord() {

		System.out.println("Enter First Name of First Employee: ");
		sc.nextLine();
		this.e1.setFirstName(sc.nextLine());

		System.out.println("Enter Last Name of First Employee: ");
		// sc.nextLine();
		this.e1.setLastName(sc.nextLine());

		System.out.println("Enter Monthly Salary of First Employee: ");
		this.e1.setMonthlySalary(sc.nextDouble());

		System.out.println("Enter First Name of Second Employee: ");
		sc.nextLine();
		this.e2.setFirstName(sc.nextLine());

		System.out.println("Enter Last Name of Second Employee: ");
		// sc.nextLine();
		this.e2.setLastName(sc.nextLine());

		System.out.println("Enter Monthly Salary of Second Employee: ");
		this.e2.setMonthlySalary(sc.nextDouble());

	}

	public void printRecord() {

		System.out.println(
				"First Employee's Full Name and Yearly Salary And New Total Annual Salary After 10% Raise. \n");

		System.out.println(e1.getFirstName() + " " + e1.getLastName() + "  " + e1.getYearlySalary() + "\n");
		System.out.println(e1.getFirstName() + " " + e1.getLastName() + "  " + e1.getRaiseSalary() + "\n");

		System.out.println("--------------------------------------------------------");
		
		
		System.out.println(
				"Second Employee's Full Name and Yearly Salary And New Total Annual Salary After 10% Raise. \n");

		System.out.println(e2.getFirstName() + " " + e2.getLastName() + "  " + e2.getYearlySalary() + "\n");
		System.out.println(e2.getFirstName() + " " + e2.getLastName() + "  " + e2.getRaiseSalary() + "\n");

	}

	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Accept Record");
		System.out.println("2.Print Record");
		System.out.print("Enter choice	:	");
		int choice = sc.nextInt();
		return choice;
	}

}