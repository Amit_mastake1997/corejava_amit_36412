package assignment;

public class Employee {

	private String firstName;
	private String lastName;
	private double monthlySalary;

	public Employee() {

		firstName = null;
		lastName = null;
		monthlySalary = 0.0;
	}

	public Employee(String firstName, String lastName, double monthlySalary) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.monthlySalary = monthlySalary;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getMonthlySalary() {
		return monthlySalary;
	}

	public void setMonthlySalary(double monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	// method to retrieve yearly salary
	public double getYearlySalary() {
		double yearlySalary = monthlySalary * 12;
		return yearlySalary;
	}

	// method to retrieve yearly salary after giving 10% raise
	public double getRaiseSalary() {
		double raise = monthlySalary * 0.1;
		double raiseSalary = (monthlySalary + raise) * 12;
		return raiseSalary;
	}

}