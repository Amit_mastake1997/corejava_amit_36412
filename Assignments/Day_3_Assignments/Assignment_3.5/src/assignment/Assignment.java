package assignment;

import java.util.Scanner;
import java.lang.Math;

class CompoundInterest {
	private int p;
	private int t;
	private double r;
	private int n;

	CompoundInterest() {
		this.p = 0;
		this.t = 0;
		this.r = 0.0;
		this.n = 0;
	}

	public CompoundInterest(int p, int t, double r, int n) {
		super();
		this.p = p;
		this.t = t;
		this.r = r;
		this.n = n;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public void calculate(int p, int t, double r, int n) {

		if ((p < 2000) && (t >= 2)) {
			r = 5;
		} else if (((p >= 2000) && (p < 6000)) && (t >= 2)) {
			r = 7;
		} else if ((p > 6000) && (t >= 1)) {
			r = 8;
		} else if (t >= 5) {
			r = 10;
		} else {
			r = 3;
		}

		double amount = p * Math.pow(1 + (r / n), n * t);
		
		double cinterest = amount - p;
		
		System.out.println("Compound Interest after " + t + " years: " + cinterest);
		System.out.println("Amount after " + t + " years: " + amount);
	}
}
public class Assignment {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CompoundInterest CI = new CompoundInterest();

		Scanner sc = new Scanner(System.in);

		System.out.print("ENTER THE P VALUE	:	");
		int principal = sc.nextInt();
		CI.setP(principal);

		System.out.print("ENTER THE T VALUE	:	");
		int time = sc.nextInt();
		CI.setT(time);

		System.out.print("ENTER THE R VALUE	:	");
		double rate = sc.nextDouble();
		CI.setR(rate);

		System.out.print("ENTER THE N VALUE	:	");
		int number = sc.nextInt();
		CI.setN(number);

		// CI.calculate(2000, 2, 0.8, 10);
		CI.calculate(principal, time, rate, number);
		sc.close();

	}

}
