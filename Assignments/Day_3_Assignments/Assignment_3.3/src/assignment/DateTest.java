package assignment;


import java.util.Scanner;


class Date {

	private int month;
	private int day;
	private int year;

	
	public Date() {
		
		month=0;
		day=0;
		year=0;
	}


	public Date(int month, int day, int year) {
		super();
		this.month = month;
		this.day = day;
		this.year = year;
	}



	public int getMonth() {
		return month;
	}



	public void setMonth(int Months){
		if(Months>=0 && Months<=12)
		  
		month=Months;
		  
		else{
		month=0; }
		}



	public int getDay() {
		return day;
	}


	public void setDay(int Days){
		if(Days>=0 && Days<=30)
		  
		day=Days;
		else{
		day=0;
		}
		}



	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}
	
	
	public void displayDate(){
		  
		System.out.printf("%d/%d/%d\n",getMonth(),getDay(),getYear());
		  
		}
	
}

public class DateTest {

	public static void main(String args[]) {

		Date date = new Date(0, 0, 0);

		Scanner input = new Scanner(System.in);

		System.out.println("Enter Month");
		int month = input.nextInt();
		date.setMonth(month);

		System.out.println("Enter Day ");
		int day = input.nextInt();
		date.setDay(day);

		System.out.println("Enter Year");
		int year = input.nextInt();
		date.setYear(year);

		date.displayDate();

	}

}