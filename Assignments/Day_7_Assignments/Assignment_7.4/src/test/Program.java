package test;

import java.util.Arrays;



class Person implements Comparable<Person>{
	private String name;
	private int age;
	private String birthDate;
	
	public Person(String name, int age, String birthDate) {
		super();
		this.name = name;
		this.age = age;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (birthDate != other.birthDate)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", birthDate=" + birthDate + "]";
	}

	@Override
	public int compareTo(Person other) {
		return this.birthDate.compareTo(other.birthDate);
	}
	
	
}

public class Program {
	public static Person[] getBirtDate() {
		Person arr [] = new Person[3];
		arr[0] = new Person("Manali", 22,"23/03/1998" );
		arr[1] = new Person("Shraddha", 24, "23/09/1996");
		arr[2] = new Person("Archana", 23 "17/01/1995");
		return arr;
	}
	
	private static void print(Person[] arr) {
		if( arr != null ) {
			for( Person date : arr )
				System.out.println(date.toString());
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Person date1 = new Person("Manali", 22, "23/03/1998");
		Person date2 = new Person("Payal", 20, "04/12/2000");

		Person[] arr = Program.getBirtDate();
		Program.print(arr);
		
		
		Arrays.sort(arr);
		Program.print(arr);

		if(date1.equals(date2)) {
            System.out.println("BirthDate is Same "); 
		}else {
            System.out.println("BirthDate is Not Same "); 

		}
	}
}
