package test;

import java.util.Arrays;

class Date implements Comparable<Date>{
	private int day;
	private int month;
	private int year;
	
	public Date() {
		// TODO Auto-generated constructor stub
	}
	public Date(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Date other) {
		return this.year - other.year;
	}
	
	
}

public class Program {
	public static Date[] getDates() {
		Date arr [] = new Date[4];
		arr[0] = new Date(1,2, 2000);
		arr[1] = new Date(24,8 ,2020);
		arr[2] = new Date(12, 10, 2008);
		arr[3] = new Date(20, 9, 2002);
		return arr;
	}
	
	private static void print(Date[] arr) {
		if( arr != null ) {
			for( Date date : arr )
				System.out.println(date.toString());
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Date date1 = new Date(26, 10 , 1976);
		Date date2 = new Date(26, 10, 1996);
		
		Date[] arr = Program.getDates();
		Program.print(arr);
		
		
		Arrays.sort(arr);
		Program.print(arr);

		if(date1.equals(date2)) {
            System.out.println("Dates are Equal "); 
		}else {
            System.out.println("Dates are Not Equal "); 

		}
	}
}