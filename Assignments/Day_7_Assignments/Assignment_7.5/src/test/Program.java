package test;

import java.util.Arrays;
import java.util.Comparator;

class Shape implements Comparable<Shape>{
	private double l;
	private double b;
	private double area;
	private double peri;
	
	public Shape() {
		// TODO Auto-generated constructor stub
	}
	
	public Shape(double l, double b) {
		super();
		this.l = l;
		this.b = b;
	}

	public double getL() {
		return l;
	}

	public void setL(double l) {
		this.l = l;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getPeri() {
		return peri;
	}

	public void setPeri(double peri) {
		this.peri = peri;
	}

	public double calculateArea() {
		return this.area = this.l*this.b;
	}
	
	public double calcuPeri() {
		return this.peri = 2 *(this.l + this.b);
	}

	@Override
	public String toString() {
		return "Shape [l=" + l + ", b=" + b + ", area=" + area + ", peri=" + peri + "]";
	}

	@Override
	public int compareTo(Shape o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
class SortByArea implements Comparator<Shape>{
	@Override
	public int compare(Shape sh1, Shape sh2) {
		return (int) (sh1.getArea() - sh2.getArea());
	}
}

class SortByPeri implements Comparator<Shape>{
	@Override
	public int compare(Shape sh1, Shape sh2) {
		return (int) (sh1.getPeri() - sh2.getPeri());
	}
}

public class Program {
	 public static Shape[] getShapes() {
		Shape arr[] = new Shape[2];
		arr[0] = new Shape(20.5,2.5);
		arr[0].calculateArea();
		arr[0].calcuPeri();
		
		arr[1] = new Shape(10.5,10.5);
		arr[1].calculateArea();
		arr[1].calcuPeri();
		return arr;
		 
	 }
	 
	 private static void print(Shape[] arr) {
			if( arr != null ) {
				for( Shape sh : arr )
					System.out.println(sh.toString());
				System.out.println();
			}
		}

	 
	 public static void main(String[] args) {
		Shape[]arr = Program.getShapes();
		Program.print(arr);
		
		Comparator<Shape> c = null;
		
		c = new SortByArea();
		Arrays.sort(arr,c);
		Program.print(arr);
		
		c = new SortByPeri();
		Arrays.sort(arr,c);
		Program.print(arr);

		
	}
}