package test;
import java.util.Arrays;


class Address implements Comparable<Address>{
	
	private String address;
	private String city;
	private String state;
	private String pincode;
	
	
	public Address() {
		// TODO Auto-generated constructor stub
	}


	public Address(String address, String city, String state, String pincode) {
		super();
		this.address = address;
		this.city = city;
		this.state = state;
		this.pincode = pincode;
	}
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (pincode == null) {
			if (other.pincode != null)
				return false;
		} else if (!pincode.equals(other.pincode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public int compareTo(Address other) {
		return this.state.compareTo(other.state);
	}

	@Override
	public String toString() {
		return "Address [address=" + address + ", city=" + city + ", state=" + state + ", pincode=" + pincode + "]";
	}
	
	
}
public class Program {
	
	public static Address[] getAddress() {
		Address arr [] = new Address[4];
		arr[0] = new Address("20,tps colony", "parli", "Maharashtra", "431520");
		arr[1] = new Address("42,Station Road", "Kulu", "HimachalPradesh", "854210");
		arr[2] = new Address("Basu Nivas", "Raipur", "Kolkata", "652386");
		arr[3] = new Address(" Gautami Uday", "Udaypur", "Rajasthan", "956214");
		return arr;
	}
	
	private static void print(Address[] arr) {
		if( arr != null ) {
			for( Address date : arr )
				System.out.println(date.toString());
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Address date1 = new Address("20,Railway Lines", "Karad", "Maharashtra", "413001");;
		Address date2 = new Address("42,Station Road", "Kulu", "HimachalPradesh", "854210");;
		
		Address[] arr = Program.getAddress();
		Program.print(arr);
		
		
		Arrays.sort(arr);
		Program.print(arr);

		if(date1.equals(date2)) {
            System.out.println("Address is Same "); 
		}else {
            System.out.println("Address is Not Same "); 

		}
	}

}
