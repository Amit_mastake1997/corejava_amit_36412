
//Que 3] Accept one integer number from command line and provide the
	//following functionalities
package test;

import java.util.Scanner;

public class Assignment2_0 {

		public static void main(String args[]) {
			int choice;
			Scanner sc = new Scanner(System.in);
			System.out.println("1]Sum_of_digits");
			System.out.println("2]Reverse_Number");
			System.out.println("3]Palindrome");
			System.out.println("4]Perfect Number");
			System.out.println("5]Strong Number");
			System.out.println("6]Armstrong Number");
			System.out.println("7]Prime_number");
			System.out.println("Enter your choice ");
			choice = sc.nextInt();
			switch (choice) {
			case 1: {
				Sum_of_digits();
			}
				break;
			case 2: {
				Reverse_Number();
			}
				break;
			case 3: {
				Palindrome();
			}
				break;
			case 4: {
				Perfect_Number();
			}
				break;
			case 5: {
				Strong_Number();
			}
				break;
			case 6: {
				Armstrong_number();
			}
				break;
			case 7: {
				Prime_number();
			}
				break;
			
			}

		}

		private static void Sum_of_digits() {
			int number, sum;
			Scanner sc = new Scanner(System.in);
			System.out.println("Eneter the Number: ");
			number = sc.nextInt();
			for (sum = 0; number != 0; number /= 10) {
				sum += number % 10;
			}
			System.out.println("Sum of digits of a number is " + sum);

		}

		private static void Reverse_Number() {
			int num, reversed = 0;
			Scanner sc = new Scanner(System.in);
			System.out.println("Eneter the Number: ");
			num = sc.nextInt();
			while (num != 0) {
				int digit = num % 10;
				reversed = reversed * 10 + digit;
				num /= 10;
			}

			System.out.println("Reversed Number: " + reversed);
		}

		private static void Palindrome() {
			int num, reversedInteger = 0, remainder, originalInteger;
			Scanner sc = new Scanner(System.in);
			System.out.println("Eneter the Number: ");
			num = sc.nextInt();
			originalInteger = num;

			// reversed integer is stored in variable
			while (num != 0) {
				remainder = num % 10;
				reversedInteger = reversedInteger * 10 + remainder;
				num /= 10;
			}

			// palindrome if orignalInteger and reversedInteger are equal
			if (originalInteger == reversedInteger)
				System.out.println(originalInteger + " is a palindrome.");
			else
				System.out.println(originalInteger + " is not a palindrome.");
		}

		private static void Perfect_Number() {
			long n, sum = 0;
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter a number");
			n = sc.nextLong();
			int i = 1;
			while (i <= n / 2) {
				if (n % i == 0) {
					sum += i;
				}
				i++;
			}
			if (sum == n) {
				System.out.println(n + " is a perfect number");
			} else
				System.out.println(n + " is not a  perfect number");
		}

		private static void Strong_Number() {
			int Number, Temp, Reminder, Sum = 0, i, Factorial;
			Scanner sc = new Scanner(System.in);

			System.out.print(" Please Enter any Number : ");
			Number = sc.nextInt();

			Temp = Number;
			while (Temp > 0) {
				Factorial = 1;
				i = 1;
				Reminder = Temp % 10;
				while (i <= Reminder) {
					Factorial = Factorial * i;
					i++;
				}
				System.out.println(" The Factorial of " + Reminder + "  =  " + Factorial);
				Sum = Sum + Factorial;
				Temp = Temp / 10;
			}

			System.out.println(" The Sum of the Factorials of a Given Number " + Number + " =  " + Sum);

			if (Number == Sum) {
				System.out.println("\n " + Number + " is a Strong Number");
			} else {
				System.out.println("\n " + Number + " is Not a Strong Number");
			}
		}

		private static void Armstrong_number() {
			int c = 0, a, temp,n;
			Scanner sc = new Scanner(System.in);
			System.out.println("Eneter the Number: ");
			n= sc.nextInt();
			temp = n;
			while (n > 0) {
				a = n % 10;
				n = n / 10;
				c = c + (a * a * a);
			}
			if (temp == c)
				System.out.println("armstrong number");
			else
				System.out.println("Not armstrong number");
		}


		private static void Prime_number() {
			int num;
			Scanner sc = new Scanner(System.in);
			System.out.println("Eneter the Number: ");
			num= sc.nextInt();
	        boolean flag = false;
	        for(int i = 2; i <= num/2; ++i)
	        {
	            // condition for nonprime number
	            if(num % i == 0)
	            {
	                flag = true;
	                break;
	            }
	        }

	        if (!flag)
	            System.out.println(num + " is a prime number.");
	        else
	            System.out.println(num + " is not a prime number.");
	    }

}
