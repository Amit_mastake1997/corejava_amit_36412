//Que6]Create a BMI (Body Mass Index) calculator that reads the user’s
//weight in pounds and height in inches (or, if you prefer,
//the user’s weight in kilograms and height in meters), then
//calculates and displays the user’s body mass index.
//The formula for calculating BMI is
//(WeightInKiloGrams)
//BMI = ----------------------------
//(HeightInMeters * HeightInMeters);
//BMI VALUES
//Underweight if BMI is less than 18.5
//Normal if BMI is in between 18.5 and 24.9
//Overweight if BMI is in between 25 and 29.9
//Obese if BMI is 30 or greater
package test;
import java.util.Scanner;
public class Assignment2_3 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Input weight in kilogram: ");
		double weight = sc.nextDouble();

		System.out.print("\nInput height in meters: ");
		double height = sc.nextDouble();
		double BMI = weight / (height * height);
		System.out.print("\nThe Body Mass Index (BMI) is " + BMI + " kg/m2");

		System.out.println("nBMI VALUES");

		if(BMI < 18.5) {
		System.out.println("You are underweight");
		}else if (BMI >= 18.5 && BMI < 24.9) {
		System.out.println("You are normal");
		}else if (BMI >= 18.5 && BMI < 24.9) {
		System.out.println("You are overweight");
		}else {
		System.out.println("You are obese");
		
		}
}
}
