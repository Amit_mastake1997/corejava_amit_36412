
//Que8] Input the current world population and the annual world population
//growth rate. Write an application to display the estimated world
//population after one, two, three, four and five years.
package test;
import java.util.Scanner;
public class Assignment2_5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		long population;
		double growthRate;

		System.out.print("Enter current world population: ");
		population = sc.nextLong();

		System.out.print("Enter annual world population growth rate: ");
		growthRate = sc.nextDouble();

		// print growth rate
		for(int i=1; i<6; i++, population *= growthRate){
		System.out.printf("%d years = %d\n", i, population);
		}
		}
}
