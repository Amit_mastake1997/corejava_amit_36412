//Que 4] Write a program for addition, subtraction, multiplication and
//division of two numbers.

package test;
import java.util.Scanner;
public class Assignment2_1 {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);


		System.out.print("Input first number: ");
		int num1 = in.nextInt();


		System.out.print("Input second number: ");
		int num2 = in.nextInt();


		System.out.println("-----------------------------");
		System.out.println(num1 + " + " + num2 + " = " + 
				(num1 + num2));


				System.out.println("-----------------------------");


				System.out.println(num1 + " - " + num2 + " = " + 
				(num1 - num2));


				System.out.println("-----------------------------");


				System.out.println(num1 + " x " + num2 + " = " + 
				(num1 * num2));


				System.out.println("-----------------------------");


				System.out.println(num1 + " / " + num2 + " = " + 
				(num1 / num2));
	}
}
